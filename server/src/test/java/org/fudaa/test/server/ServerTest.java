package org.fudaa.test.server;

import static org.junit.Assert.*;

public class ServerTest {
  @org.junit.Test
  public void getVersion() {
    assertEquals(1, new Server().getVersion());
  }
}
