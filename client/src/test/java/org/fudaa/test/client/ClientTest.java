package org.fudaa.test.client;

import static org.junit.Assert.*;

public class ClientTest {

  @org.junit.Test
  public void getVersion() {
    assertEquals(1, new Client().getVersion());
  }
}
