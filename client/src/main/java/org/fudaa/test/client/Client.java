package org.fudaa.test.client;

import org.fudaa.test.server.Server;

public class Client {
  private Server server = new Server();

  public Server getServer() {
    return server;
  }

  public void setServer(Server server) {
    this.server = server;
  }

  public int getVersion() {
    return server.getVersion();
  }

  public void displayVersion() {
    System.out.println(server.getVersion());
  }

  public static void main(String[] args) {
    new Client().displayVersion();
  }
}
